atom.commands.add 'atom-text-editor',
  'custom:insert-comment-title-1': ->
    atom.workspace.getActiveTextEditor()?.insertText('###############################################################################\n# Comment Title \n###############################################################################')
    atom.workspace.getActiveTextEditor()?.moveUp(2)
    atom.workspace.getActiveTextEditor()?.moveRight(3)
    atom.workspace.getActiveTextEditor()?.selectToEndOfLine()

atom.commands.add 'atom-text-editor',
  'custom:insert-comment-title-2': ->
    atom.workspace.getActiveTextEditor()?.insertText('# ===================================================\n# Comment Subtitle \n# ===================================================')
    atom.workspace.getActiveTextEditor()?.moveUp(2)
    atom.workspace.getActiveTextEditor()?.moveRight(3)
    atom.workspace.getActiveTextEditor()?.selectToEndOfLine()

atom.commands.add 'atom-text-editor',
  'custom:insert-comment-title-3': ->
    atom.workspace.getActiveTextEditor()?.insertText('# -> ')
