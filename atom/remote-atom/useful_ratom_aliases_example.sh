#!/usr/bin/zsh

function_draner_ratom_mustache_js () {
    # example : draner_ratom_mustache_js oeuvres aquarelles

    if [ -z "$1" ]; then
      echo -e "\nPlease call '$0 <category>' to run this command properly.\n"
      return 1
    fi

    if [ -z "$2" ]; then
      # no filename is given => opening all js files
      echo "Ratom all '$1' js files.\n"
      ratom assets/js/mustache/$1/*
    else
      # a file name is given => opening the given file
      echo "Ratom assets/js/mustache/$1/$2.js"
      ratom assets/js/mustache/$1/$2.js
    fi
}

function_draner_ratom_mustache_html () {
    # example : function_draner_ratom_html artiste techniques

    if [ -z "$1" ]; then
      echo -e "\nPlease call '$0 <category>' to run this command properly.\n"
      return 1
    fi

    if [ -z "$2" ]; then
      # no filename is given => opening all js files
      echo "Ratom all '$1' html files.\n"
      ratom $1/*
    else
      # a file name is given => opening the given file
      echo "Ratom $1/$2.html"
      ratom $1/$2.html
    fi
}

function_draner_tree_images () {
    # example : function_draner_ratom_html artiste techniques

    if [ -z "$1" ]; then
      echo -e "\nPlease call '$0 <category>' to run this command properly.\n"
      return 1
    fi

    if [ -z "$2" ]; then
      # tree the whole category folder
      echo "Tree whole '$1' folder\n"
      tree -v images/$1
    else
      # tree the whole subcategory folder
      echo "Tree whole '$1/$2' folder\n"
      tree -v images/$1/$2
    fi
}

function_draner_ratom_all () {
    # example : function_draner_ratom_all oeuvres aquarelles
    function_draner_ratom_mustache_js $1 $2
    function_draner_ratom_mustache_html $1 $2
    function_draner_tree_images $1 $2
}

alias draner_ratom_mustache="cddraner && function_draner_ratom_mustache_js"
alias draner_ratom_html="cddraner && function_draner_ratom_html"
alias draner_ratom_all="cddraner && function_draner_ratom_all"
alias draner_tree_images="cddraner && function_draner_tree_images"
